import unittest
import pandas as pd
from fairness.rebalancing import variable_quantization


class VariableQuantization(unittest.TestCase):

    def test_variable_quantization_1(self):
        df = pd.DataFrame({'Attribute': pd.Series([0, 10, 3, 4, 5, 6])})
        tuple_list_result = [(0.0, 2.5), (2.5, 5.0), (5.0, 7.5), (7.5, 10.0)]

        self.assertEqual(variable_quantization(df, 'Attribute'), tuple_list_result)
