import unittest
import pandas as pd
from fairness.rebalancing import return_biased_attributes


class ReturnBiasedAttributesTest(unittest.TestCase):

    def test_return_biased_attributes(self):
        attribute = pd.Series(['Gender', 'Race', 'Sex'])
        metric_value = pd.Series([0.75, 1.24, 1])

        df = pd.DataFrame({'Attribute': attribute, 'Metric value': metric_value})
        list_biased_attributes = return_biased_attributes(df)

        self.assertEqual(len(list_biased_attributes), 1)
        self.assertEqual(list_biased_attributes[0], 'Gender')

    def test_return_biased_attributes_2(self):
        attribute = pd.Series(['Gender', 'Race', 'Sex'])
        metric_value = pd.Series([0.81, 1.24, 1])

        df = pd.DataFrame({'Attribute': attribute, 'Metric value': metric_value})
        list_biased_attributes = return_biased_attributes(df)

        self.assertEqual(len(list_biased_attributes), 0)
