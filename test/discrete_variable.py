import unittest

import pandas as pd

from fairness.rebalancing import is_variable_discrete


class DiscreteVariableTest(unittest.TestCase):

    def test_discrete_variable_1(self):
        integer_list = range(1, 10)
        df = pd.DataFrame({'Integer': pd.Series(integer_list)})

        boolean_result = is_variable_discrete(df, 'Integer')
        self.assertEqual(boolean_result, True)

    def test_discrete_variable_2(self):
        variable = [0.8, range(1, 19)]
        df = pd.DataFrame({'Variable': pd.Series(variable)})

        boolean_result = is_variable_discrete(df, 'Variable')
        self.assertEqual(boolean_result, False)
