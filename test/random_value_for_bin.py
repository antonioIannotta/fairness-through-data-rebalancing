import unittest
from fairness.rebalancing import return_random_value_for_bin


class RandomValueBinTest(unittest.TestCase):

    def test_random_value_bin(self):
        bin = (1, 5)
        value = return_random_value_for_bin(bin)
        boolean_value = 1 < value < 5
        self.assertEqual(boolean_value, True)
