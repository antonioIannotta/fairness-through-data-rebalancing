import unittest
import pandas as pd
from fairness.rebalancing import is_variable_continuous


class ContinuousVariableTest(unittest.TestCase):

    def test_continuous_variable_1(self):
        variable = [0.1, 0.2, 0.3, 0.5]
        df = pd.DataFrame({'Variable': pd.Series(variable)})

        boolean_result = is_variable_continuous(df, 'Variable')
        self.assertEqual(boolean_result, True)